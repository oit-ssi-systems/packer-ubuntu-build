#!/bin/bash
#
# by Zach Hill <zach.hill@duke.edu>
# modified by Chris Collins for Ansible-configured Ubuntu VMs
#
# This script is to be run during the preseed/late_command portion
# of the Ubuntu kickstart process to set up the system. This script
# is tailored for the self-admin image distributed by Duke OIT.
#

## Add linux-crashdump and enable kdump/kexec
export DEBIAN_FRONTEND=noninteractive;
apt-get install -y linux-crashdump
dpkg-reconfigure kdump-tools

## disable http proxy which gets used during install time on build network
sed -i '/^Acquire::http::Proxy/d' /etc/apt/apt.conf

## Update datasource list so cloud-init doesn't look around
sed -i '/datasource_list/c\datasource_list:\ \[\ NoCloud\,\ None\ \]' /etc/cloud/cloud.cfg.d/90_dpkg.cfg

## We need this to fix a bug in cloud-init 0.7.7 - hopefully fixed soon
sed -i "s/'network-config': {}}/'network-config': None}/" /usr/lib/python3/dist-packages/cloudinit/sources/DataSourceNoCloud.py

## Remove preseed hostname from system
sh -c "echo > /etc/hostname"

## Enable DNS for SSHD
sh -c "echo UseDNS yes >> /etc/ssh/sshd_config"

## Fix interfaces for networking
sed -i '/eth0/d' /etc/network/interfaces

## allow cloud-init to set network-interfaces up
if [[ -f /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg ]]; then
	rm -v /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg;
fi

# remove proxy from snap configuration
if [[ -f /etc/systemd/system/snapd.service.d/snap_proxy.conf ]]; then
	rm -v /etc/systemd/system/snapd.service.d/snap_proxy.conf;
fi

## wipe out installer's cloud-init leftovers
if [[ -f /etc/cloud/cloud.cfg.d/99-installer.cfg ]]; then
	rm -v /etc/cloud/cloud.cfg.d/99-installer.cfg;
fi

## drop netplan config left from the auto-installer
if [[ -f /etc/netplan/00-installer-config.yaml ]]; then
	rm -v /etc/netplan/00-installer-config.yaml;
fi

cloud-init clean --logs

## Disable persisten network names
sed -i '/^GRUB_CMDLINE_LINUX=/c\GRUB_CMDLINE_LINUX="net.ifnames=0 biosdevname=0"' /etc/default/grub && update-grub

## Enable persistent journald config
mkdir /var/log/journal
systemd-tmpfiles --create --prefix /var/log/journal
systemctl restart systemd-journald

exit 0
