#!/bin/bash

DISK_USAGE_BEFORE_CLEANUP=$(df -h)

# Remove Ansible and its dependencies.
# Remove kernel stuff we don't need
# remove provisioning sudoers file
rm /etc/sudoers.d/provisioning

# Delete old kernels

# Clean package cache
echo "Cleaning up extra files"
apt-get -y autoremove
apt-get -y clean

# Zero out the rest of the free space using dd, then delete the written file.
echo "Reclaming free space on disk"

# Ideally we'd know here if we're on storage with optimized zero writes but
# I'm guessing we're not or that vmware doesn't care and just records the zeros
# making a thin disk as big as a thick one. -- nbc9
# dd if=/dev/zero of=/EMPTY bs=1M  || echo "dd exit code $? is suppressed"
# rm -f /EMPTY

# this probably doesn't do anything
fstrim -av

# Add `sync` so Packer doesn't quit too early, before the large file is deleted.
sync

echo "==> Disk usage before cleanup"
echo "${DISK_USAGE_BEFORE_CLEANUP}"
echo "==> Disk usage after cleanup"
df -h
echo "==> LVM cleanup"
/sbin/lvchange -v -an ubuntuos/placeholder
/sbin/lvremove -v -y ubuntuos/placeholder
echo "==> LVM Layout"
pvdisplay
vgdisplay
lvdisplay
