packer {
  required_version = ">= 1.7.0"
  required_plugins {
    vsphere = {
      version = ">= 1.2.7"
      source  = "github.com/hashicorp/vsphere"
    }
    ansible = {
      version = ">= 1.1.1"
      source  = "github.com/hashicorp/ansible"
    }
  }
}

variable "build_hash" {
  type    = string
  default = "${env("BUILD_HASH")}"
}

variable "build_hash_short" {
  type    = string
  default = "${env("BUILD_HASH_SHORT")}"
}

variable "build_output" {
  type    = string
  default = "${env("BUILD_OUTPUT")}"
}

variable "build_os" {
  type    = string
  default = "${env("BUILD_OS")}"
}

variable "datastore-iso-path" {
  type    = string
  default = "[OIT-NAS-FE11-NFS-ISOS]clockworks_isos/linux/"
}

variable "template-domainname" {
  type    = string
  default = "oit.duke.edu"
}

variable "template-hostname" {
  type    = string
  default = "${env("TEMPLATE_HOSTNAME")}"
}

variable "template-password" {
  type    = string
  default = "${env("TEMPLATE_PASSWORD")}"
}

variable "template-password-crypt" {
  type    = string
  default = "${env("TEMPLATE_PASSWORD_CRYPT")}"
}

variable "template-username" {
  type    = string
  default = "${env("TEMPLATE_USERNAME")}"
}

variable "template-ssh-pubkey" {
  type    = string
  default = "${env("TEMPLATE_SSH_PUBKEY")}"
}

variable "vsphere_cluster" {
  type    = string
  default = "${env("GOVC_CLUSTER")}"
}

variable "vsphere_datacenter" {
  type    = string
  default = "${env("GOVC_DATACENTER")}"
}

variable "vsphere_datastore" {
  type    = string
  default = "${env("GOVC_DATASTORE")}"
}

variable "vsphere_folder" {
  type    = string
  default = "${env("GOVC_FOLDER")}"
}

variable "vsphere_network" {
  type    = string
  default = "${env("GOVC_NETWORK")}"
}

variable "vsphere_password" {
  type    = string
  default = "${env("GOVC_PASSWORD")}"
}

variable "vsphere_resource_pool" {
  type    = string
  default = "${env("GOVC_RESOURCE_POOL")}"
}

variable "vsphere_server" {
  type    = string
  default = "${env("GOVC_URL")}"
}

variable "vsphere_username" {
  type    = string
  default = "${env("GOVC_USERNAME")}"
}

variable "template-os-version" {
  type = string
}

variable "template-info-file" {
  type    = string
  default = "/etc/duke/template-info.txt"
}

variable "installer-iso" {
  type = string
}

variable "installer-iso-url" {
  type = string
}

variable "installer-iso-sha256" {
  type = string
}

variable "ansible-python-interpreter" {
  type = string
}

variable "content-library" {
  type    = string
  default = "${env("GOVC_CONTENT_LIBRARY")}"
}

variable "final-template-name" {
  type    = string
  default = "${env("FINAL_TEMPLATE_NAME")}"
}

source "vsphere-iso" "ubuntu-vm" {
  CPU_hot_plug         = true
  CPU_limit            = -1
  CPUs                 = 1
  RAM                  = "1024"
  RAM_hot_plug         = true
  boot_command         = ["c<wait>", "setparams 'packer-build'<enter>", "linuxefi /install/vmlinuz", " auto=true", " priority=critical", " console-setup/ask_detect=false", " fb=false", " file=/floppy/preseed.cfg", " noprompt quiet<enter>", "initrdefi /install/initrd.gz<enter>", "boot<enter>"]
  boot_wait            = "10s"
  cluster              = var.vsphere_cluster
  datacenter           = var.vsphere_datacenter
  datastore            = var.vsphere_datastore
  disk_controller_type = ["pvscsi"]
  firmware             = "efi"
  floppy_files         = ["packer-output/preseed.cfg"]
  folder               = "${var.vsphere_folder}"
  guest_os_type        = "ubuntu64Guest"
  insecure_connection  = "true"
  iso_paths            = ["${var.datastore-iso-path}/${var.installer-iso}"]
  network_adapters {
    network      = "${var.vsphere_network}"
    network_card = "vmxnet3"
  }
  password         = "${var.vsphere_password}"
  resource_pool    = "${var.vsphere_resource_pool}"
  shutdown_command = "echo \"${var.template-password}\"|sudo -S shutdown -P now"
  ssh_password     = "${var.template-password}"
  ssh_timeout      = "20m"
  ssh_username     = "${var.template-username}"
  storage {
    disk_size = 51200
  }
  username       = "${var.vsphere_username}"
  vcenter_server = "${var.vsphere_server}"
  vm_name        = "packer-${var.template-os-version}-${var.build_hash_short}"
  vm_version     = 11

  content_library_destination {
    name    = var.final-template-name
    library = var.content-library
    ovf     = true
    destroy = true
  }

}

source "vsphere-iso" "ubuntu-legacy-ai-vm" {
  CPU_hot_plug = true
  CPU_limit    = -1
  CPUs         = 1
  RAM          = "2048"
  RAM_hot_plug = true
  boot_command = [
    "c",
    "setparams 'packer-build'<enter>",
    "linuxefi /casper/vmlinuz root=/dev/sr0 initrd=/casper/initrd autoinstall<enter>",
    "initrdefi /casper/initrd<enter>",
    "boot<enter>"
  ]
  boot_wait            = "2s"
  cluster              = var.vsphere_cluster
  datacenter           = var.vsphere_datacenter
  datastore            = var.vsphere_datastore
  disk_controller_type = ["pvscsi"]
  firmware             = "efi"
  cd_content = {
    "meta-data" = ""
    "user-data" = templatefile("auto-install/auto-install.tpl", { var = var })
  }
  cd_label            = "cidata"
  folder              = "${var.vsphere_folder}"
  guest_os_type       = "ubuntu64Guest"
  insecure_connection = "true"
  iso_paths           = ["${var.datastore-iso-path}/${var.installer-iso}"]
  network_adapters {
    network      = "${var.vsphere_network}"
    network_card = "vmxnet3"
  }
  password             = "${var.vsphere_password}"
  resource_pool        = "${var.vsphere_resource_pool}"
  shutdown_command     = "echo \"${var.template-password}\"|sudo -S shutdown -P now"
  ssh_password         = "${var.template-password}"
  ssh_private_key_file = "~/.ssh/id_ed25519"
  ssh_timeout          = "20m"
  ip_settle_timeout    = "5m"
  ssh_username         = "${var.template-username}"
  storage {
    disk_size = 51200
  }
  username       = "${var.vsphere_username}"
  vcenter_server = "${var.vsphere_server}"
  vm_name        = "packer-${var.template-os-version}-${var.build_hash_short}"
  vm_version     = 11

  content_library_destination {
    name    = var.final-template-name
    library = var.content-library
    ovf     = true
    destroy = true
  }

}

source "vsphere-iso" "ubuntu-ai-vm" {
  CPU_hot_plug = true
  CPU_limit    = -1
  CPUs         = 1
  RAM          = "2048"
  RAM_hot_plug = true
  boot_command = [
    "c",
    "setparams 'packer-build'<enter>",
    "linux /casper/vmlinuz root=/dev/sr0 initrd=/casper/initrd autoinstall<enter>",
    "initrd /casper/initrd<enter>",
    "boot<enter>"
  ]
  boot_wait            = "2s"
  cluster              = var.vsphere_cluster
  datacenter           = var.vsphere_datacenter
  datastore            = var.vsphere_datastore
  disk_controller_type = ["pvscsi"]
  firmware             = "efi"
  cd_content = {
    "meta-data" = ""
    "user-data" = templatefile("auto-install/auto-install.tpl", { var = var })
  }
  cd_label            = "cidata"
  folder              = "${var.vsphere_folder}"
  guest_os_type       = "ubuntu64Guest"
  insecure_connection = "true"
  iso_paths           = ["${var.datastore-iso-path}/${var.installer-iso}"]
  network_adapters {
    network      = "${var.vsphere_network}"
    network_card = "vmxnet3"
  }
  password             = "${var.vsphere_password}"
  resource_pool        = "${var.vsphere_resource_pool}"
  shutdown_command     = "echo \"${var.template-password}\"|sudo -S shutdown -P now"
  ssh_password         = "${var.template-password}"
  ssh_private_key_file = "~/.ssh/id_ed25519"
  ssh_timeout          = "20m"
  ip_settle_timeout    = "5m"
  ssh_username         = "${var.template-username}"
  storage {
    disk_size = 51200
  }
  username       = "${var.vsphere_username}"
  vcenter_server = "${var.vsphere_server}"
  vm_name        = "packer-${var.template-os-version}-${var.build_hash_short}"
  vm_version     = 11

  content_library_destination {
    name    = var.final-template-name
    library = var.content-library
    ovf     = true
    destroy = true
  }

}

build {
  sources = ["source.vsphere-iso.ubuntu-legacy-ai-vm"]

  provisioner "shell" {
    environment_vars = ["TEMPLATE_INFO_FILE=${var.template-info-file}", "BUILD_HASH=${var.build_hash}", "BUILD_HASH_SHORT=${var.build_hash_short}", "BUILD_OS=${var.build_os}"]
    execute_command  = "echo ${var.template-password} | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
    script           = "/scripts/template-info.sh"
  }

  provisioner "shell" {
    environment_vars = ["TEMPLATE_USERNAME=${var.template-username}"]
    execute_command  = "echo ${var.template-password} | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
    script           = "/scripts/prep-ansible.sh"
  }

  provisioner "ansible" {
    user            = "${var.template-username}"
    extra_arguments = ["-e", "ansible_python_interpreter=${var.ansible-python-interpreter}", "--scp-extra-args", "'-O'"]
    playbook_file   = "playbooks/packer-provisioning/packer-ubuntu.yaml"
  }

  provisioner "shell" {
    execute_command = "echo ${var.template-password} | sudo sh -c '{{ .Vars }} {{ .Path }}'"
    script          = "scripts/in-target-cleanup.sh"
  }

  provisioner "shell" {
    execute_command = "echo ${var.template-password} | sudo sh -c '{{ .Vars }} {{ .Path }}'"
    script          = "scripts/guest-cleanup.sh"
  }

  post-processor "manifest" {
    output     = "${var.build_output}/manifest.json"
    strip_path = true
  }

}

build {
  sources = ["source.vsphere-iso.ubuntu-ai-vm"]

  provisioner "shell" {
    environment_vars = ["TEMPLATE_INFO_FILE=${var.template-info-file}", "BUILD_HASH=${var.build_hash}", "BUILD_HASH_SHORT=${var.build_hash_short}", "BUILD_OS=${var.build_os}"]
    execute_command  = "echo ${var.template-password} | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
    script           = "/scripts/template-info.sh"
  }

  provisioner "shell" {
    environment_vars = ["TEMPLATE_USERNAME=${var.template-username}"]
    execute_command  = "echo ${var.template-password} | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
    script           = "/scripts/prep-ansible.sh"
  }

  provisioner "ansible" {
    user            = "${var.template-username}"
    extra_arguments = ["-e", "ansible_python_interpreter=${var.ansible-python-interpreter}", "--scp-extra-args", "'-O'"]
    playbook_file   = "playbooks/packer-provisioning/packer-ubuntu.yaml"
  }

  provisioner "shell" {
    execute_command = "echo ${var.template-password} | sudo sh -c '{{ .Vars }} {{ .Path }}'"
    script          = "scripts/in-target-cleanup.sh"
  }

  provisioner "shell" {
    execute_command = "echo ${var.template-password} | sudo sh -c '{{ .Vars }} {{ .Path }}'"
    script          = "scripts/guest-cleanup.sh"
  }

  post-processor "manifest" {
    output     = "${var.build_output}/manifest.json"
    strip_path = true
  }

}

build {
  sources = ["source.vsphere-iso.ubuntu-vm"]

  provisioner "shell" {
    environment_vars = ["TEMPLATE_INFO_FILE=${var.template-info-file}", "BUILD_HASH=${var.build_hash}", "BUILD_HASH_SHORT=${var.build_hash_short}", "BUILD_OS=${var.build_os}"]
    execute_command  = "echo ${var.template-password} | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
    script           = "/scripts/template-info.sh"
  }

  provisioner "shell" {
    environment_vars = ["TEMPLATE_USERNAME=${var.template-username}"]
    execute_command  = "echo ${var.template-password} | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
    script           = "/scripts/prep-ansible.sh"
  }

  provisioner "ansible" {
    user            = "${var.template-username}"
    extra_arguments = ["-e", "ansible_python_interpreter=${var.ansible-python-interpreter}", "--scp-extra-args", "'-O'"]
    playbook_file   = "playbooks/packer-provisioning/packer-ubuntu.yaml"
  }

  provisioner "shell" {
    execute_command = "echo ${var.template-password} | sudo sh -c '{{ .Vars }} {{ .Path }}'"
    script          = "scripts/in-target-cleanup.sh"
  }

  provisioner "shell" {
    execute_command = "echo ${var.template-password} | sudo sh -c '{{ .Vars }} {{ .Path }}'"
    script          = "scripts/guest-cleanup.sh"
  }

  post-processor "manifest" {
    output     = "${var.build_output}/manifest.json"
    strip_path = true
  }

}
