#cloud-config
timezone: US/Eastern
apt:
  preserve_sources_list: false
  primary:
    - arches: [default]
      uri: http://archive.linux.duke.edu/ubuntu

autoinstall:

  early-commands:
    - systemctl stop ssh

  late-commands:
    - sed -i -e 's/^#\?PermitRootLogin.*/PermitRootLogin prohibit-password/g' /target/etc/ssh/sshd_config

  apt:
    preserve_sources_list: false
    geoip: false
    primary:
      - arches: [default]
        uri: http://archive.linux.duke.edu/ubuntu

  identity:
    hostname: ${var.template-hostname}
    password: ${var.template-password-crypt}
    realname: packer template user
    username: packer

  proxy: http://proxy.oit.duke.edu:3128

  ssh:
    install-server: true
    authorized-keys:
      - ${var.template-ssh-pubkey}

  storage:
    config:
    - type: disk
      id: sda
      ptable: gpt
      path: /dev/sda
      preserve: false
      name: main_disk
      grub_device: false

    - type: partition
      id: sda1
      device: sda
      size: 1G
      flag: boot
      number: 1
      grub_device: true
    - type: format
      id: efi-fs
      fstype: fat32
      volume: sda1
      preserve: false
    - type: mount
      id: efi-mount
      path: /boot/efi
      device: efi-fs
      
    - type: partition
      id: sda2
      device: sda
      size: 1G
      number: 2
    - type: format
      id: boot-fs
      fstype: ext4
      volume: sda2
      preserve: false
    - type: mount
      id: boot-mount
      path: /boot
      device: boot-fs

    - type: partition
      id: sda3
      device: sda
      size: -1
      wipe: superblock
      flag: ''
      number: 3
    - type: lvm_volgroup
      id: vg0
      name: ubuntuos
      devices:
      - sda3

    - type: lvm_partition
      id: root-lv
      name: root-lv
      volgroup: vg0
      size: 25G
    - type: format
      id: root-fs
      fstype: xfs
      volume: root-lv
    - type: mount
      id: root-mount
      path: /
      device: root-fs

    - type: lvm_partition
      id: home-lv
      name: home-lv
      volgroup: vg0
      size: 5G
    - type: format
      id: home-fs
      fstype: xfs
      volume: home-lv
    - path: /home
      device: home-fs
      type: mount
      id: home-mount

    - type: lvm_partition
      id: srv-lv
      name: srv
      volgroup: vg0
      size: 5G
    - type: format
      id: srv-fs
      fstype: xfs
      volume: srv-lv
    - type: mount
      id: srv-mount
      path: /srv
      device: srv-fs

    - name: placeholder
      volgroup: vg0
      size: -1
      type: lvm_partition
      id: placeholder-lv
    - fstype: xfs
      volume: placeholder-lv
      preserve: false
      type: format
      id: placeholder-fs
  version: 1